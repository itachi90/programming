package Find_The_Celebrity;

/**
 * Created by yichi_zhang on 9/30/15.
 */
public class Solution extends Relation {
  public int findCelebrity(int n) {
    int candidate = 0;
    for (int i = 1; i < n; i++) {
      if (knows(candidate, i)) {
        candidate = i;
      }
    }
    for (int j = 0; j < n; j++) {
      if (j != candidate && (knows(candidate, j) || !knows(j, candidate))) {
        return -1;
      }
    }
    return candidate;
  }
}
