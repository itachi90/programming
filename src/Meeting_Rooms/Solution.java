package Meeting_Rooms;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public boolean canAttendMeetings(Interval[] intervals) {
    Arrays.sort(intervals, new comp());
    int last = -1;
    for (Interval i : intervals) {
      if (last > 0 && i.start < last) {
        return false;
      }
      last = i.end;
    }
    return true;
  }
  public class comp implements Comparator<Interval> {
    public int compare(Interval v1, Interval v2) {
      return v1.start - v2.start;
    }
  }
}
