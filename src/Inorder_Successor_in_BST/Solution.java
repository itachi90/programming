package Inorder_Successor_in_BST;

/**
 * Created by yichi_zhang on 9/29/15.
 *
 * Given a binary search tree and a node in it, find the in-order successor of that node in the BST.
 * Note: If the given node has no in-order successor in the tree, return null.
 */

/**
 * Definition for a binary tree node.
 */
class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode(int x) {
    val = x;
  }
}


public class Solution {
  public TreeNode prev = null;
  public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
    if (root == null) {
      return null;
    }

    TreeNode left = inorderSuccessor(root.left, p);
    if (left != null) {
      return left;
    }

    if (prev == p) {
      return root;
    }
    prev = root;
    TreeNode right = inorderSuccessor(root.right, p);
    if (right != null) {
      return right;
    }
    return null;
  }
}
