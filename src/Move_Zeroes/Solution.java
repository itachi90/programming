package Move_Zeroes;

/**
 * Created by yichi_zhang on 9/29/15.
 */
public class Solution {
  public void moveZeroes(int[] nums) {
    if (nums == null || nums.length == 0) {
      return;
    }
    for (int i = 0 ; i < nums.length; i++) {
      for (int j = i; j < nums.length; ) {
        if (nums[j] == 0) {
          int k = j + 1;
          while (k < nums.length && nums[k] == 0) {
            k++;
          }
          if (k >= nums.length) {
            break;
          }
          swap(nums, j, k);
          j = k;
        } else {
          j++;
        }
      }
    }
  }
  public void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
  }
}
