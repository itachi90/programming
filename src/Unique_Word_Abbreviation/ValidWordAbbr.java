package Unique_Word_Abbreviation;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class ValidWordAbbr {
  public Map<String, Set<String>> map;

  public ValidWordAbbr(String[] dictionary) {
    this.map = new HashMap<>();
    for (String s : dictionary) {
      String abbr = abbrev(s);
      if (map.containsKey(abbr)) {
        map.get(abbr).add(s);
      } else {
        Set<String> set = new HashSet<>();
        set.add(s);
        map.put(abbr, set);
      }
    }
  }

  public boolean isUnique(String word) {
    return !map.containsKey(abbrev(word)) || map.get(abbrev(word)).size() == 0 || (map.get(abbrev(word)).size() == 1 && map.get(abbrev(word)).contains(word));
  }

  public String abbrev(String word) {
    if (word == null || word.length() <= 2) {
      return word;
    }
    return word.charAt(0) + String.valueOf(word.length() - 1) + word.charAt(word.length() - 1);
  }
}
