package Verify_Preorder_Sequence_in_Binary_Search_Tree;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public boolean verifyPreorder(int[] preorder) {
    int root = Integer.MIN_VALUE;
    int ptr = -1;
    for (int i = 0; i < preorder.length; i++) {
      if (preorder[i] < root) {
        return false;
      }
      while (ptr >= 0 && preorder[i] > preorder[ptr]) {
        root = preorder[ptr--];
      }
      preorder[++ptr] = preorder[i];
    }
    return true;
  }
}
