package H_Index_II;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public int hIndex(int[] citations) {
    if (citations == null || citations.length == 0) {
      return 0;
    }
    int l = 0, r = citations.length - 1;
    while (l + 1 < r) {
      int mid = l + (r - l) / 2;
      if (citations[mid] >= citations.length - mid) {
        r = mid;
      } else {
        l = mid;
      }
    }
    if (citations[l] >= citations.length - l) {
      return citations.length - l;
    }
    if (citations[r] >=  citations.length - r) {
      return citations.length -r;
    }
    return 0;
  }
}
