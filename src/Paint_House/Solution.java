package Paint_House;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class Solution {
  public int minCost(int[][] costs) {
    if (costs == null || costs.length == 0) {
      return 0;
    }
    int[][] mincost = new int[costs.length][3];
    for (int i = 0; i < 3; i++) {
      mincost[0][i] = costs[0][i];
    }
    for (int i = 1; i < costs.length; i++) {
      mincost[i][0] = Math.min(mincost[i - 1][1], mincost[i - 1][2]) + costs[i][0];
      mincost[i][1] = Math.min(mincost[i - 1][0], mincost[i - 1][2]) + costs[i][1];
      mincost[i][2] = Math.min(mincost[i - 1][1], mincost[i - 1][0]) + costs[i][2];
    }
    int min = Integer.MAX_VALUE;
    for (int i = 0; i < 3; i++) {
      min = Math.min(min, mincost[costs.length - 1][i]);
    }
    return min;
  }
}
