package Three_Sum_Smaller;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public int threeSumSmaller(int[] nums, int target) {
    int count = 0;
    if (nums == null || nums.length == 0) return count;
    Arrays.sort(nums);
    for (int i = 0; i < nums.length; i++) {
      //if (i > 0 && nums[i] == nums[i - 1]) continue;
      int j = i + 1;
      int k = nums.length - 1;
      while (j < k) {
        if (nums[i] + nums[j] + nums[k] >= target && k > j) {
          k--;
        } else {
          count += k - j;
          j++;
        }
      }
    }
    return count;
  }
}
