package Strobogrammatic_Number;

/**
 * Created by yichi_zhang on 10/5/15.
 */
public class Solution {
  public boolean isStrobogrammatic(String num) {
    Map<Character, Character> dict = new HashMap<>();
    dict.put('0', '0');
    dict.put('1', '1');
    dict.put('6', '9');
    dict.put('8', '8');
    dict.put('9', '6');
    int i = 0;
    int j = num.length() - 1;
    while (i <= j) {
      if (!dict.containsKey(num.charAt(i)) || (dict.get(num.charAt(i)) != num.charAt(j))) {
        return false;
      }
      i++;
      j--;
    }
    return true;
  }
}
