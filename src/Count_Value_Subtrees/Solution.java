package Count_Value_Subtrees;

/**
 * Created by yichi_zhang on 10/4/15.
 */
public class Solution {
  public int count = 0;
  public int countUnivalSubtrees(TreeNode root) {
    helper(root);
    return count;
  }
  public int helper(TreeNode root) {
    if (root == null) {
      return 0;
    }
    int leftUni = helper(root.left);
    int rightUni = helper(root.right);
    if (leftUni == -1 ||rightUni  == -1) {
      return -1;
    } else if (root.left != null && root.val != root.left.val) {
      return -1;
    } else if (root.right != null && root.val != root.right.val) {
      return -1;
    } else {
      count++;
    }
    return count;
  }
}
