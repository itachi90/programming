package Meeting_Room_II;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public int minMeetingRooms(Interval[] intervals) {
    if (intervals == null || intervals.length == 0) {
      return 0;
    }
    Pair[] schedule = new Pair[intervals.length * 2];
    for (int i = 0; i < schedule.length; i+=2) {
      schedule[i] = new Pair(intervals[i / 2].start, 1);
      schedule[i + 1] = new Pair(intervals[i / 2].end, -1);
    }
    Arrays.sort(schedule, new comp());
    int count = 0;
    int max = Integer.MIN_VALUE;
    for (Pair i : schedule) {
      count += i.weight;
      max = Math.max(max, count);
    }
    return max;
  }
  public class Pair {
    public int time;
    public int weight;
    public Pair(int time, int weight) {
      this.time = time;
      this.weight = weight;
    }
  }
  public class comp implements Comparator<Pair> {
    public int compare(Pair p1, Pair p2) {
      if (p1.time != p2.time) {
        return p1.time - p2.time;
      } else return p1.weight - p2.weight;
    }
  }
}
