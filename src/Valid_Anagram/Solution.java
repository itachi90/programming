package Valid_Anagram;

/**
 * Created by yichi_zhang on 10/5/15.
 */
public class Solution {
  public boolean isAnagram(String s, String t) {
    if (s == null || t == null) {
      return false;
    }
    char[] ca1 = s.toCharArray();
    char[] ca2 = t.toCharArray();
    Arrays.sort(ca1);
    Arrays.sort(ca2);
    String s1 = new String(ca1);
    String s2 = new String(ca2);
    return s1.equals(s2);
  }
}
