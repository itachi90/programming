package PeekIterator;

import java.util.Iterator;

/**
 * Created by yichi_zhang on 9/29/15.
 */
class PeekingIterator implements Iterator<Integer> {
  public Iterator<Integer> itor;
  public Integer _next;
  public PeekingIterator(Iterator<Integer> iterator) {
    // initialize any member here.
    this.itor = iterator;
    this._next = itor.next();
  }

  // Returns the next element in the iteration without advancing the iterator.
  public Integer peek() {
    return this._next;
  }

  // hasNext() and next() should behave the same as in the Iterator interface.
  // Override them if needed.
  @Override
  public Integer next() {
    Integer ret = this._next;
    if (this.itor.hasNext()) {
      this._next = this.itor.next();
    } else {
      this._next = null;
    }
    return ret;
  }

  @Override
  public void remove() {

  }

  @Override
  public boolean hasNext() {
    return this._next != null;
  }
}
