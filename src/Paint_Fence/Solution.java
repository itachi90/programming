package Paint_Fence;

/**
 * Created by yichi_zhang on 9/30/15.
 */
public class Solution {
  public int numWays(int n, int k) {
    if (n == 0) {
      return 0;
    }
    if (n == 1) {
      return k;
    }
    int[] ways = new int[2];
    ways[0] = k * (k - 1); //diff color
    ways[1] = k; //same color
    for (int i = 2; i < n; i++) {
      int temp = ways[0];
      ways[0] = (ways[0] + ways[1]) * (k - 1);
      ways[1] = temp;
    }
    return ways[0] + ways[1];
  }
}
