package Missing_Number;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class Solution {
  public int missingNumber(int[] nums) {
    int length = nums.length;
    int xor = 0;
    for (int i = 1; i <= length; i++) {
      xor = xor ^ i;
    }
    for (int i : nums) {
      xor = xor ^ i;
    }
    return xor;
  }
}
