package Closest_Binary_Search_Tree_Value;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class Solution {
  public int closestValue(TreeNode root, double target) {
    int a = root.val;
    TreeNode kid = target < a ? root.left : root.right;
    if (kid == null) return a;
    int b = closestValue(kid, target);
    return Math.abs(a - target) < Math.abs(b - target) ? a : b;
  }
}
