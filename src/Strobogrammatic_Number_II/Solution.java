package Strobogrammatic_Number_II;

/**
 * Created by yichi_zhang on 10/5/15.
 */
public class Solution {
  public List<String> findStrobogrammatic(int n) {
    Map<Character, Character> dict = new HashMap<>();
    dict.put('0', '0');
    dict.put('1', '1');
    dict.put('6', '9');
    dict.put('8', '8');
    dict.put('9', '6');

    List<Character> list = new ArrayList<>();
    list.add('0');
    list.add('1');
    list.add('6');
    list.add('8');
    list.add('9');

    List<String> res = new ArrayList<>();
    boolean odd = (n % 2 == 1);
    helper(res, "", list, n / 2, odd, dict);
    return res;
  }

  public void helper(List<String> res, String cur, List<Character> candidates, int n, boolean odd, Map<Character, Character> dict) {
    if (n == 0) {
      if (odd) {
        char[] center = new char[]{'0', '1', '8'};
        for (Character c : center) {
          if (cur.length() >= 1 && cur.charAt(0) == '0') {
            return;
          }
          cur += c;
          res.add(new String(completeNum(cur, dict, odd)));
          cur = cur.substring(0, cur.length() - 1);
        }
      } else {
        if (cur.length() >= 1 && cur.charAt(0) == '0') {
          return;
        }
        res.add(new String(completeNum(cur, dict, odd)));
      }
      return;
    }
    for (int i = 0; i < candidates.size(); i++) {
      cur += candidates.get(i);
      helper(res, cur, candidates, n - 1, odd, dict);
      cur = cur.substring(0, cur.length() - 1);
    }
  }
  public String completeNum(String s, Map<Character, Character> dict, boolean odd) {
    for (int i = odd ? s.length() - 2 : s.length() - 1; i >= 0; i--) {
      s += dict.get(s.charAt(i));
    }
    return s;
  }
}
