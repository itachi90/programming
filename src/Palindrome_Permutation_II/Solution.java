package Palindrome_Permutation_II;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yichi_zhang on 10/6/15.
 */
public class Solution {
  public List<String> generatePalindromes(String s) {
    Map<Character, Integer> dict = new HashMap<>();
    List<String> res = new ArrayList<>();
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (dict.containsKey(c)) {
        dict.put(c, dict.get(c) + 1);
      } else {
        dict.put(c, 1);
      }
    }
    int count = 0;
    for (int i : dict.values()) {
      if (i % 2 == 1) {
        count++;
      }
    }
    if (count > 1) {
      return res;
    }

    helper(res, dict, "", s.length() / 2, count);
    return res;
  }

  public void helper(List<String> res, Map<Character, Integer> dict, String cur, int n, int odd) {
    if (n == 0) {
      if (odd == 1) {
        for (Map.Entry<Character, Integer> e : dict.entrySet()) {
          if ((int)e.getValue() == 1) {
            res.add(complete(cur + e.getKey(), odd));
            return;
          }
        }
      } else {
        res.add(complete(cur, odd));
        return;
      }
    }

    for (Map.Entry<Character, Integer> e : dict.entrySet()) {
      if ((int)e.getValue() >= 2) {
        char key = (char)e.getKey();
        int value = (int)e.getValue();
        dict.put(key, value - 2);
        helper(res, dict, cur + key, n - 1, odd);
        dict.put(key, value);
      }
    }
  }

  public String complete(String s, int odd) {
    String ret = s;
    for (int i = odd == 1 ? s.length() - 2 : s.length() - 1; i >= 0; i--) {
      ret += s.charAt(i);
    }
    return ret;
  }
}
