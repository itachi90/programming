package Different_Ways_to_Add_Parentheses;

/**
 * Created by yichi_zhang on 10/6/15.
 */
public class Solution {
  public List<Integer> diffWaysToCompute(String input) {
    List<Integer> res = new ArrayList<>();
    if (input == null || input.length() == 0) {
      return res;
    }
    for (int i = 0; i < input.length(); i++) {
      char c = input.charAt(i);
      if (c == '-' || c == '+' || c == '*') {
        List<Integer> op1 = diffWaysToCompute(input.substring(0, i));
        List<Integer> op2 = diffWaysToCompute(input.substring(i + 1));
        for (int x : op1) {
          for (int y : op2) {
            if (c == '-') {
              res.add(x - y);
            } else if (c == '+') {
              res.add(x + y);
            } else {
              res.add(x * y);
            }
          }
        }
      }
    }
    if (res.size() == 0) {
      res.add(Integer.parseInt(input));
    }
    return res;
  }
}
