package Shortest_Word_Distance;

/**
 * Created by yichi_zhang on 10/4/15.
 */
public class Solution {
  public int shortestDistance(String[] words, String word1, String word2) {
    if (words == null || words.length == 0) {
      return 0;
    }
    int p1 = -1, p2 = -1, distance = Integer.MAX_VALUE;
    for (int i = 0; i < words.length; i++) {
      if (words[i].equals(word1)) {
        p1 = i;
      }
      if (words[i].equals(word2)) {
        p2 = i;
      }
      if (p1 != -1 && p2 != -1) {
        distance = Math.min(Math.abs(p1 - p2), distance);
      }
    }
    return distance;
  }
}
