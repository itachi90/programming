package Ugly_Number_II;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public int nthUglyNumber(int n) {
    Queue<Integer> q1 = new LinkedList<>();
    Queue<Integer> q2 = new LinkedList<>();
    Queue<Integer> q3 = new LinkedList<>();
    q1.add(2);
    q2.add(3);
    q3.add(5);
    if (n == 1) return 1;
    int nth = 2;
    for (int i = 2; i <= n; i++) {
      int min = q1.peek();
      min = Math.min(Math.min(min, q2.peek()), q3.peek());
      if (min == q1.peek()) {
        nth = q1.poll();
        q1.add(nth < Integer.MAX_VALUE / 2 ? nth * 2 : Integer.MAX_VALUE);
        q2.add(nth < Integer.MAX_VALUE / 3 ? nth * 3 : Integer.MAX_VALUE);
        q3.add(nth < Integer.MAX_VALUE / 5 ? nth * 5 : Integer.MAX_VALUE);
      }
      else if (min == q2.peek()) {
        nth = q2.poll();
        q2.add(nth < Integer.MAX_VALUE / 3 ? nth * 3 : Integer.MAX_VALUE);
        q3.add(nth < Integer.MAX_VALUE / 5 ? nth * 5 : Integer.MAX_VALUE);
      }
      else if (min == q3.peek()) {
        nth = q3.poll();
        q3.add(nth < Integer.MAX_VALUE / 5 ? nth * 5 : Integer.MAX_VALUE);
      }
    }
    return nth;
  }
}
