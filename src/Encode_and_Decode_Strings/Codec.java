package Encode_and_Decode_Strings;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class Codec {

  // Encodes a list of strings to a single string.
  public String encode(List<String> strs) {
    StringBuilder ret = new StringBuilder();
    for (String s : strs) {
      ret.append(s.length());
      ret.append('#');
      ret.append(s);
    }
    return ret.toString();
  }

  // Decodes a single string to a list of strings.

  public List<String> decode(String s) {
    List<String> ret = new ArrayList<>();
    while (s.length() > 0) {
      int slash = s.indexOf('#');
      int length = Integer.parseInt(s.substring(0, slash));
      ret.add(s.substring(slash + 1, slash + 1 + length));
      s = s.substring(slash + 1 + length);
    }
    return ret;
  }
}
