package First_Bad_Version;

/**
 * Created by yichi_zhang on 9/29/15.
 */
public class Solution extends VersionControl {
  public int firstBadVersion(int n) {
    return search(1, n);
  }
  public int search(int l, int r) {

    while (l < r - 1) {
      int mid = l + (r - l) / 2;
      if (isBadVersion(mid)) {
        r = mid;
      } else {
        l = mid;
      }
    }
    if (!isBadVersion(l) && isBadVersion(r)) {
      return r;
    }
    return l;
  }
}
