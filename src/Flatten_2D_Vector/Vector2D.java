package Flatten_2D_Vector;

/**
 * Created by yichi_zhang on 10/4/15.
 */
public class Vector2D {
  public int listPtr;
  public int inListPtr;
  public List<List<Integer>> vec;

  public Vector2D(List<List<Integer>> vec2d) {
    this.vec = vec2d;
    this.listPtr = 0;
    this.inListPtr = 0;
  }

  public int next() {

    int ret = vec.get(listPtr).get(inListPtr++);
    return ret;
  }

  public boolean hasNext() {
    while (listPtr < vec.size() && inListPtr >= vec.get(listPtr).size()) {
      listPtr++;
      inListPtr = 0;
    }
    return listPtr < vec.size() && inListPtr < vec.get(listPtr).size();
  }
}
