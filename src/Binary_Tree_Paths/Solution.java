package Binary_Tree_Paths;

/**
 * Created by yichi_zhang on 10/1/15.
 */
public class Solution {
  public List<String> binaryTreePaths(TreeNode root) {
    List<String> res = new ArrayList<>();
    String cur = "";
    helper(res, cur, root);
    return res;
  }
  public void helper(List<String> res, String cur, TreeNode root) {
    if (root == null) return;
    if (root != null && root.left == null && root.right == null) {
      String path = cur + String.valueOf(root.val);
      res.add(path);
      return;
    }
    int length = cur.length();
    cur += String.valueOf(root.val) + "->";
    if (root.left != null)
      helper(res, cur, root.left);
    if (root.right != null)
      helper(res, cur, root.right);
    cur = cur.substring(0, length);
  }
}
