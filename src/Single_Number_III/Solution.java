package Single_Number_III;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public int[] singleNumber(int[] nums) {
    int[] ret = new int[2];
    if (nums == null || nums.length == 0) {
      return ret;
    }
    int axorb = 0;
    for (int i : nums) {
      axorb = axorb ^ i;
    }
    ret[0] = axorb;
    ret[1] = axorb;
    int lastbit = axorb & ~(axorb - 1);
    for (int i : nums) {
      if ((i & lastbit) == 0) {
        ret[0] = ret[0] ^ i;
      }
      else {
        ret[1] = ret[1] ^ i;
      }
    }
    return ret;
  }
}
