package Product_of_Array_Except_Self;

/**
 * Created by yichi_zhang on 10/6/15.
 */
public class Solution {
  public int[] productExceptSelf(int[] nums) {
    int[] ret = new int[nums.length];
    if (nums.length <= 1) {
      return new int[]{1};
    }
    Arrays.fill(ret, 1);
    int left = 1;
    for (int i = 1; i < nums.length; i++) {
      ret[i] = left * nums[i - 1];
      left = left * nums[i - 1];
    }
    int right = 1;
    for (int i = nums.length - 2; i >= 0; i--) {
      right = right * nums[i + 1];
      ret[i] = ret[i] * right;
    }
    return ret;
  }
}
