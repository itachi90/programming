package Group_Shifted_Strings;

/**
 * Created by yichi_zhang on 10/4/15.
 */
public class Solution {
  public List<List<String>> groupStrings(String[] strings) {
    List<List<String>> res = new ArrayList<>();
    if (strings == null || strings.length == 0) {
      return res;
    }
    Map<String, List<String>> map = new HashMap<>();
    for (String s : strings) {
      String key = hashKey(s);
      if (map.containsKey(key)) {
        map.get(key).add(s);
      } else {
        List<String> list = new ArrayList<>();
        list.add(s);
        map.put(key, list);
      }
    }
    for (List<String> l : map.values()) {
      Collections.sort(l);
      res.add(l);
    }
    return res;
  }

  public String hashKey(String str) {
    if (str.length() == 0) return "";
    StringBuilder s = new StringBuilder();
    int shift = str.charAt(0) - 'a';
    s.append('a');
    for (int i = 1; i < str.length(); i++) {
      if (str.charAt(i) - 'a' < shift) {
        s.append(str.charAt(i) + 26 - shift);
      } else {
        s.append(str.charAt(i) - shift);
      }
    }
    return s.toString();
  }
}
