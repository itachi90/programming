package Word_Pattern;

/**
 * Created by yichi_zhang on 10/5/15.
 */
public class Solution {
  public boolean wordPattern(String pattern, String str) {
    String[] strs = str.split(" ");
    if (pattern.length() != strs.length) {
      return false;
    }
    Map<Character, String> dict = new HashMap<>();
    Map<String, Character> dict2 = new HashMap<>();
    for (int i = 0; i < strs.length; i++) {
      char c = pattern.charAt(i);
      if (dict.containsKey(c) && !dict.get(c).equals(strs[i])) {
        return false;
      }
      if (dict2.containsKey(strs[i]) && !dict2.get(strs[i]).equals(c)) {
        return false;
      }
      dict.put(c, strs[i]);
      dict2.put(strs[i], c);
    }
    return true;
  }
}
