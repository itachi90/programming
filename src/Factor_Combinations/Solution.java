package Factor_Combinations;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public List<List<Integer>> getFactors(int n) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> cur = new ArrayList<>();
    if (n <= 1) {
      return res;
    }
    helper(res, cur, n, 0);
    return res;
  }
  public void helper(List<List<Integer>> res, List<Integer> cur, int n, int size) {
    if (n == 1 && size > 1) {
      res.add(new ArrayList<Integer>(cur));
      return;
    }
    if (size > 0 && cur.get(size - 1) > n) {
      return;
    }
    for (int i = 2; i <= n; i++) {
      if (size > 0 && cur.get(size - 1) > i) {
        continue;
      }
      if (n % i != 0) {
        continue;
      }
      cur.add(i);
      helper(res, cur, n / i, size + 1);
      cur.remove(cur.size() - 1);
    }
  }
}
