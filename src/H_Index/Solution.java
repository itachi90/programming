package H_Index;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yichi_zhang on 9/30/15.
 */
public class Solution {
  public int hIndex(int[] citations) {
    if (citations == null || citations.length == 0) {
      return 0;
    }
    Arrays.sort(citations);
    Map<Integer, Integer> indexHigh = new HashMap<>();
    int count = 0;
    int max = 0;
    for (int i = citations.length - 1; i >= 0; i--) {
      indexHigh.put(citations[i], ++count);
    }
    for (int i : citations) {
      for (int j = 0; j <= indexHigh.get(i); j++) {
        if (j <= i) {// so that j of all paper have at least j citatitons is true
          max = Math.max(max, j);
        }
      }
    }
    return max;
  }
}
