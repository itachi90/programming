package Palindrome_Permutation;

/**
 * Created by yichi_zhang on 10/3/15.
 */
public class Solution {
  public boolean canPermutePalindrome(String s) {
    Map<Character, Integer> dict = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {
      if (dict.containsKey(s.charAt(i)) && dict.get(s.charAt(i)) == 1) {
        dict.put(s.charAt(i), 0);
      } else {
        dict.put(s.charAt(i), 1);
      }
    }
    int count = 0;
    for (int i : dict.values()) {
      if (i % 2 != 0) {
        count++;
      }
    }
    return count <= 1;
  }
}
