package Perfect_Squares;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yichi_zhang on 9/29/15.
 */
public class Solution {
  public int numSquares(int n) {
    int[] num = new int[n + 1];
    Arrays.fill(num, Integer.MAX_VALUE);
    List<Integer> squares = getSquares(n);
    num[0] = 0;
    for (int j = 0; j < squares.size(); j++)
      for (int i = squares.get(j); i <= n; i++) {
        num[i] = Math.min(num[i - squares.get(j)] + 1, num[i]);
      }
    return num[n];
  }

  public List<Integer> getSquares(int n) {
    List<Integer> ret = new ArrayList<>();
    int i = 1;
    while (i * i <= n) {
      ret.add(i * i);
      i++;
    }
    return ret;
  }
  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.numSquares(13));
  }
}


