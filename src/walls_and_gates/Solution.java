package walls_and_gates;

/**
 * Created by yichi_zhang on 9/29/15.
 */

import java.util.LinkedList;
import java.util.Queue;

/**
 * You are given a m x n 2D grid initialized with these three possible values.
 *
 * -1 - A wall or an obstacle. 0 - A gate. INF - Infinity means an empty room. We use the value 231
 * - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than
 * 2147483647. Fill each empty room with the distance to its nearest gate. If it is impossible to
 * reach a gate, it should be filled with INF.
 *
 * For example, given the 2D grid: INF  -1  0  INF INF INF INF  -1 INF  -1 INF  -1 0  -1 INF INF
 * After running your function, the 2D grid should be: 3  -1   0   1 2   2   1  -1 1  -1   2  -1 0
 * -1   3   4
 */

public class Solution {
  public static int INF = 2147483647;

  public static void wallsAndGates(int[][] rooms) {
    if (rooms == null || rooms.length == 0) {
      return;
    }
    for (int i = 0; i < rooms.length; i++) {
      for (int j = 0; j < rooms[0].length; j++) {
        if (rooms[i][j] == 0) {
          Queue<Pos> list = new LinkedList<>();
          list.add(new Pos(j, i));
          gridBfs(rooms, list, new int[rooms[0].length][rooms.length], 0);
        }
      }
    }

  }

  public static void gridBfs(int[][] rooms, Queue<Pos> list, int[][] visited, int distance) {
    while (!list.isEmpty()) {
      int length = list.size();
      for (int i = 0; i < length; i++) {
        Pos position = list.poll();
        rooms[position.y][position.x] = Math.min(distance, rooms[position.y][position.x]);
        visited[position.y][position.x] = 1;
        enqueue(rooms, list, position.x + 1, position.y, visited);
        enqueue(rooms, list, position.x - 1, position.y, visited);
        enqueue(rooms, list, position.x, position.y + 1, visited);
        enqueue(rooms, list, position.x, position.y - 1, visited);
      }
      distance += 1;
    }
  }

  public static void enqueue(int[][] rooms, Queue<Pos> list, int x, int y, int[][] visited) {
    if (x >= 0 && x < rooms[0].length && y >= 0 && y < rooms.length) {
      if (rooms[y][x] > 0 && visited[y][x] != 1) {
        list.add(new Pos(x, y));
      }
    }
  }

  public static void main(String[] args) {
    int[][] grid = new int[][]{ new int[]{INF, -1, 0, INF}, new int[]{INF, INF, INF, -1}, new int[]{INF, -1, INF, -1}, new int[]{0, -1, INF, INF}};
    wallsAndGates(grid);
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid[0].length; j++) {
        System.out.print(grid[i][j] + "  ");
      }
      System.out.print("\n");
    }
  }

  public static class Pos {
    public int x;
    public int y;

    public Pos(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }
}
