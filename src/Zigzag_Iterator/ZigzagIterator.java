package Zigzag_Iterator;

/**
 * Created by yichi_zhang on 9/30/15.
 */
public class ZigzagIterator {
  private int ptr;
  private List<Integer> v;

  public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
    this.v = new LinkedList<>();
    this.ptr = 0;
    int pv1 = 0;
    int pv2 = 0;
    int count = 0;
    while (v1 != null && v2 != null && pv1 < v1.size() && pv2 < v2.size()) {
      if (count % 2 == 0) {
        v.add(v1.get(pv1++));
      } else {
        v.add(v2.get(pv2++));
      }
      count++;
    }
    while (v1 != null && pv1 < v1.size()) {
      v.add(v1.get(pv1++));
    }
    while (v2 != null && pv2 < v2.size()) {
      v.add(v2.get(pv2++));
    }
  }

  public int next() {
    return v.get(ptr++);
  }

  public boolean hasNext() {
    return ptr < v.size();
  }
}
