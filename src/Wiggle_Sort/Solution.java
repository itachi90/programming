package Wiggle_Sort;

/**
 * Created by yichi_zhang on 9/30/15.
 */
public class Solution {
  public void wiggleSort(int[] nums) {
    if (nums == null || nums.length == 0) {
      return;
    }
    Arrays.sort(nums);
    int mid = (nums.length + 1 ) / 2;
    for (int i = 1; i < nums.length; i+=2) {
      pop(nums, i);
    }
  }
  public void pop(int[] nums, int k) {
    for (int i = nums.length - 1; i > k; i--) {
      int temp = nums[i];
      nums[i] = nums[i - 1];
      nums[i - 1] = temp;
    }
  }
}
