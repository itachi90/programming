package Shortest_Word_Distance_II;

/**
 * Created by yichi_zhang on 10/4/15.
 */
public class WordDistance {
  public Map<String, List<Integer>> index = new HashMap<>();

  public WordDistance(String[] words) {
    for (int i = 0; i < words.length; i++) {
      if (index.containsKey(words[i])) {
        index.get(words[i]).add(i);
      } else {
        List<Integer> list = new ArrayList<>();
        list.add(i);
        index.put(words[i], list);
      }
    }
  }

  public int shortest(String word1, String word2) {
    List<Integer> list1 = index.get(word1);
    List<Integer> list2 = index.get(word2);
    int ptr1 = 0, ptr2 = 0, distance = Integer.MAX_VALUE;
    while (ptr1 < list1.size() && ptr2 < list2.size()) {
      distance = Math.min(distance, Math.abs(list1.get(ptr1) - list2.get(ptr2)));
      if (list1.get(ptr1) < list2.get(ptr2)) {
        ptr1++;
      } else {
        ptr2++;
      }
    }
    return distance;
  }
}
